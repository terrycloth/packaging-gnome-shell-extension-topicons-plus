# RPM packaging of the GNOME Shell extension TopIcons Plus

This package spec is for TopIcons Plus, which has its own GNOME Shell
[extension page](https://extensions.gnome.org/extension/1031/topicons/)
and source code [repository](https://github.com/phocean/TopIcons-plus).

Many applications, such as chat clients, downloaders, and some media
players, are meant to run long-term in the background even after you
close their window.  These applications remain accessible by adding an
icon to the GNOME Shell Legacy Tray. However, the Legacy Tray is hidden
until you push your mouse into the lower-left of the screen and click on
the small tab that appears. TopIcons Plus brings all icons back to the
top panel, so that it's easier to keep track of apps running in the
background. You also get some options to control the look and feel: You
can leave the icons in full color, or dynamically convert them to
grayscale, etc.



## Bug reports and feature requests

Report any issues with TopIcons Plus itself on the project's
[GitHub](https://github.com/phocean/TopIcons-plus/issues).

Report issues specific to this package on the
[Red Hat Bugzilla](https://bugzilla.redhat.com/buglist.cgi?product=Fedora&component=gnome-shell-extension-topicons-plus).



## License

Everything specific to this repository uses the MIT License.

TopIcons Plus itself uses the GNU GPL version 2.
